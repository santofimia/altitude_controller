#ifndef altitude_controller
#define altitude_controller

#include "pid_control.h"

/*!*************************************************************************************
 *  \class     altitude_controller
 *
 *  \brief     Altitude Controller PID
 *
 *  \details   This class is in charge of control altitude level using PID class.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "xmlfilereader.h"


class AltitudeController
{
public:

  void setUp();

  void start();

  void stop();

  void getOutput(float *dh);
  void setReference(float ref_h);
  void setFeedback(float height);

  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  AltitudeController();

  //! Destructor.
  ~AltitudeController();

private:

  PID PID_Z2Dz;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string altconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name

  float pid_z2dz_kp;
  float pid_z2dz_ki;
  float pid_z2dz_kd;
  bool pid_z2dz_enablesat;
  float pid_z2dz_satmax;
  float pid_z2dz_satmin;
  bool pid_z2dz_enableantiwp;
  float pid_z2dz_kw;
  bool pid_z2dz_enableff;
  float pid_z2dz_ffmass;
  float pid_z2dz_fffactor;


};
#endif 
