#include "altitude_controller.h"

//Constructor
AltitudeController::AltitudeController()
{
    std::cout << "Constructor: AltitudeController" << std::endl;

}

//Destructor
AltitudeController::~AltitudeController() {}

bool AltitudeController::readConfigs(std::string configFile)
{

    try
    {


    XMLFileReader my_xml_reader(configFile);

    /*********************************  Position Z Controller ( from Z to Dz ) **************************************************/

    // Gain
    pid_z2dz_kp = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Gain:Kp");
    pid_z2dz_ki = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Gain:Ki");
    pid_z2dz_kd = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Gain:Kd");
    pid_z2dz_enablesat = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Saturation:enable_saturation");
    pid_z2dz_satmax = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Saturation:SatMax");
    pid_z2dz_satmin = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Saturation:SatMin");
    pid_z2dz_enableantiwp = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Anti_wind_up:enable_anti_wind_up");
    pid_z2dz_kw = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:Anti_wind_up:Kw");
    pid_z2dz_enableff = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:FeedForward:enable_feedforward");
    pid_z2dz_ffmass = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:FeedForward:mass");
    pid_z2dz_fffactor = my_xml_reader.readDoubleValue("Altitude_Controller:PID_Z2Dz:FeedForward:factor");


    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}

void AltitudeController::setUp()
{

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~alt_config_file", altconfigFile);
    if ( altconfigFile.length() == 0)
    {
        altconfigFile="altitude_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+altconfigFile);

    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }
    std::cout << "Constructor: AltitudeController...Exit" << std::endl;

}

void AltitudeController::start()
{

    // Reset PID
    PID_Z2Dz.reset();

}

void AltitudeController::stop()
{

}

void AltitudeController::setFeedback(float height ){

    PID_Z2Dz.setFeedback(height);

}
void AltitudeController::setReference(float ref_h){

    PID_Z2Dz.setReference(ref_h);

}

void AltitudeController::getOutput(float *dh){

    /*********************************  Z Controller ( from Z to dZ ) ************************************************/

    PID_Z2Dz.setGains(pid_z2dz_kp,pid_z2dz_ki,pid_z2dz_kd);
    PID_Z2Dz.enableMaxOutput(pid_z2dz_enablesat,pid_z2dz_satmin,pid_z2dz_satmax);
    PID_Z2Dz.enableAntiWindup(pid_z2dz_enableantiwp,pid_z2dz_kw);
    PID_Z2Dz.enableFeedForward(pid_z2dz_enableff,pid_z2dz_ffmass,pid_z2dz_fffactor);

    *dh = PID_Z2Dz.getOutput();
}

